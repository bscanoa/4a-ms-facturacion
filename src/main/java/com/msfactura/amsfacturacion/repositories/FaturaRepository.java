package com.msfactura.amsfacturacion.repositories;
import com.msfactura.amsfacturacion.models.Factura;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FaturaRepository extends MongoRepository <Factura, String> { }
