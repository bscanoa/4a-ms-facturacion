package com.msfactura.amsfacturacion.repositories;
import com.msfactura.amsfacturacion.models.Reserva;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ReservaRepository extends MongoRepository <Reserva, String> { }
