package com.msfactura.amsfacturacion.models;

import org.bson.codecs.DateCodec;
import org.springframework.data.annotation.Id;
import java.util.Date;

public class Factura {
    @Id
    private String username;
    private String email;
    private Date fechaEntrega;
    private Double totalNeto;
    private Double totalAdicionales;
    private Double iva;
    private Double total;
    private String adicionales;

    public Factura(String username, String email, Date fechaEntrega, Double totalNeto, Double totalAdicionales, Double iva, Double total, String adicionales){
        this.username = username;
        this.email =  email;
        this.fechaEntrega = fechaEntrega;
        this.totalNeto = totalNeto;
        this.totalAdicionales = totalAdicionales;
        this.iva = iva;
        this.total = total
        this.adicionales = adicionales;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Double getTotalNeto() {
        return totalNeto;
    }

    public void setTotalNeto(Double totalNeto) {
        this.totalNeto = totalNeto;
    }

    public Double getTotalAdicionales() {
        return totalAdicionales;
    }

    public void setTotalAdicionales(Double totalAdicionales) {
        this.totalAdicionales = totalAdicionales;
    }

    public Double getIva() {
        return iva;
    }

    public void setIva(Double iva) {
        this.iva = iva;
    }

    public String getAdicionales() {
        return adicionales;
    }

    public void setAdicionales(String adicionales) {
        this.adicionales = adicionales;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
