package com.msfactura.amsfacturacion.models;

import org.bson.codecs.DateCodec;
import org.springframework.data.annotation.Id;
import java.util.Date;

public class Reserva {
    private Date fechaCreado;
    private Date fechaRecogida;
    private String carName;
    private String username;
    private Double diasDeUso;
    private Double total;

    public Reserva(Date fechaCreado, Date fechaRecogida, String carName, String username, Double diasDeUso, Double total){
        this.fechaCreado = fechaCreado;
        this.fechaRecogida = fechaRecogida;
        this.carName = carName;
        this.username = username;
        this.diasDeUso = diasDeUso;
        this.total = total;
    }

    public Date getFechaCreado() {
        return fechaCreado;
    }

    public void setFechaCreado(Date fechaCreado) {
        this.fechaCreado = fechaCreado;
    }

    public Date getFechaRecogida() {
        return fechaRecogida;
    }

    public void setFechaRecogida(Date fechaRecogida) {
        this.fechaRecogida = fechaRecogida;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Double getDiasDeUso() {
        return diasDeUso;
    }

    public void setDiasDeUso(Double diasDeUso) {
        this.diasDeUso = diasDeUso;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}

